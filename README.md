# Platy Centralized Genomics Resources

Let's keep some metadata on where all our various data lives

Table of Contents
=================

* [Platy Centralized Genomics Resources](#platy-centralized-genomics-resources)
* [Table of Contents](#table-of-contents)
   * [Context](#context)
      * [Goals](#goals)
      * [Notes from what I am seeing](#notes-from-what-i-am-seeing)
      * [Reproducibility](#reproducibility)
      * [Extra files](#extra-files)
   * [Looking into what data we have](#looking-into-what-data-we-have)
      * [Data that needs to be found, especially raw data](#data-that-needs-to-be-found-especially-raw-data)
         * [Ours](#ours)
      * [Data that raises questions](#data-that-raises-questions)
      * [Data to deal with](#data-to-deal-with)
      * [Data folder in taped storage](#data-folder-in-taped-storage)
         * [DNA folder](#dna-folder)
         * [RNA folder](#rna-folder)
   * [Older genome/transcriptome versions](#older-genometranscriptome-versions)
      * [List of genome version](#list-of-genome-version)
      * [State of various genome assembly, on taped storage especially](#state-of-various-genome-assembly-on-taped-storage-especially)
      * [Transcriptomes from other labs WIP](#transcriptomes-from-other-labs-wip)
   * [Other annelids genomes WIP](#other-annelids-genomes-wip)
      * [Papers to look at](#papers-to-look-at)
         * [Platy papers with genomic raw data](#platy-papers-with-genomic-raw-data)
         * [Non Platy](#non-platy)
      * [Marine annelids WIP](#marine-annelids-wip)
         * [Owenia fusiformis WIP](#owenia-fusiformis-wip)
         * [Capitella telata WIP](#capitella-telata-wip)
         * [Pristina leydyi](#pristina-leydyi)
         * [Enchytraeus japonensis](#enchytraeus-japonensis)
      * [Others non-marine annelids WIP](#others-non-marine-annelids-wip)
   * [Data obtained from collaboration that should be cited](#data-obtained-from-collaboration-that-should-be-cited)
* [Table of Contents](#table-of-contents-1)

<!-- Created by https://github.com/ekalinin/github-markdown-toc -->

## Context

The Arendt lab and others have generated a wealth of data that will be of interest to the community and the lab itself.
Unfortunately, there is a huge organizational problem to track what data has been generated and where it lives. For example I have
no clues about where our *P. megalops* genome is currently.

It gets compounded by the fact that:
- it is not always clear what stage/sexes are used
- some data comes from 3rd party (labs or sequencing servers)
- the `/g/arendt` folder is a mess, plus what is on the tape storage
- the lab has apparently a tendency to create empty folders that never become filled (genome folder)

To solve this
- Some effort has been done for the 10x sequencing with a [Google Spreadsheet](https://docs.google.com/spreadsheets/d/1QbzNBQiD07mh8M4lh7ScEAhxvSrKL2SZN0dgo_vqisg)


### Goals

* Navigate and index the lab files for genomics resources
    * Google spreadsheet
    * Whatever exist on STOCKS
    * Whatever lives on the storage server
    * What we got from collaborators

* The output should have:
    * stage and sex if existing
    * strain / origin
    * where the raw files are
    * where intermediates files can be found
    * notes / tags that are relevant

### Notes from what I am seeing

- [x] Check what people are using for STAR genome alignments. `alignIntronMax` at 1000000 seems high but it goes to 2M in humans
- [ ] I am seeing Fastq files in non-obvious places. Just running `find` would be a huge help on a variety of common file extensions. Search for Hi-C / omni as well
- [ ] Some people have had their stuff taped. I would need to explore it a bit. Currently here are the locations with taped data:
  * on tape storage `0_TAPED/1_ALUMNI` has many `txt` folders including Achim's data.
  * on both `/g/arendt` and the taped storage
- [ ] Find login details for Dovetail.
- [x] Look at Genecore budget. From 2019-20. Processed
- [ ] Talk to Conrad for shallow sequence of other worms. Talk to Conrad for that.
- [ ] One Teissmar-Raible paper did RT-qPCR for validation: https://elifesciences.org/articles/41556 and has some proteome data
- [ ] Fabi's proteomics data would be quite cool to add
- [ ] **Tessmar Raible has done FACS sorting + single cell on Platy: https://elifesciences.org/articles/66144#abstract**
- [ ] **cool clearing protocol**: https://doi.org/10.1126/sciadv.aba0365

###  Reproducibility

I need this in my `/etc/fstab` on WSL2.

    //arendt.embl.de/arendt /g/arendt       cifs    user=cros,uid=1000,gid=1000,forceuid,forcegid,vers=2.1,credentials=/root/.cifs,nofail,_netdev 0 0
    //t2arendt.embl.de/t2arendt /mnt/arendt_t2      cifs    user=cros,uid=1000,gid=1000,forceuid,forcegid,vers=2.1,credentials=/root/.cifs,nofail,_netdev 0 0

`/root/.cifs` contents

    username=FOO
    password=BAR

### Extra files

- `./auxiliary_info/Genecore_orders_2017_12_01-2024_05_23.xlsx` has across multiple sheets our Genecore orders and prefixes.
- `./auxiliary_info/Export_for_Dataset_DEFAULT.xlsx` has an export of the Arendt Datasets from 2024-05-28
- `./auxiliary_info/12864_2023_9602_MOESM2_ESM.xlsx` is a list of datasets from Eve Gazave's paper

## Looking into what data we have

TODO: create a master list of all our resources including other labs

### Data that needs to be found, especially raw data

#### Ours

- [ ] Raw data for the genome project which I would love to see realigned / any kind of support for transcripts. See Kevin's Github
- [x] P. megalops genome
  * Got a Dropbox link from Kevin. Read-only share for all platy genomes: https://www.dropbox.com/scl/fo/fv9avislnudrjd2cvif8r/ACsGxQAOm2Wiy7mGTexWPCo?rlkey=sj2pk4n723i1e78wv4o22ez6k&st=dey014kk&dl=0
  * Assembly on taped storage. Illumina on STOCKS
  * No transcriptomics data exist
- [x] Jekely 1M transcripts. Solved by [their website](https://jekelylab.ex.ac.uk/sequences/bmc_genomics_2013.html)
- [x] Anything related to Hi-C
  * Is on STOCK with OmniC technology: DTG-139 DTG-483 for megalops; DTG-1214 for Platy
  * the `/g/arendt/HiC` data has some stuff but old from 2019. PduDpnIISperm6
  * Dovetail did Hi-C library as well. May be called HTC something. Omni-C. Looks like Illumina sequencing
  * Leslie sharing a `Template molarity calculator`` spreadsheet may not be used
- [ ] Anything related to SNPs
  * raw reads missing. Directly submitted to Genecore. From our lab only?
  * Kevin said he would upload it, on his server
- [x] Missing Pacbio HiFi from Duygu? See down Pacbio
- [x] *Whitney Laboratory for Marine Bioscience, Florida USA for PacBio long-read sequencing* See [this repo](https://github.com/mickey-spongebob/platynereis-sequencing/tree/master/long-read_sequencing/pacbio_LR) Some early Pacbio data from Leonid Moroz.
- [ ] Achim data I see mentioned around in [Kevin's Github](https://github.com/mickey-spongebob/platynereis-sequencing)



From Eve Gazave's 2023 transcriptome. I am adding the supp file 2 which has a `datasets` sheet: `./auxiliary_info/12864_2023_9602_MOESM2_ESM.xlsx`. We have paired end for Chou and Gazave of various sizes, single end for Schenk (and Gazave but not included), and some Nanopore long read for Gazave. **Kevin did use some of it already for his transcriptome**
- Schneider lab: https://pdumbase2020.icob.sinica.edu.tw/platynereis/controller.php?action=download
  - [ ]  *PdumBase: a transcriptome database and research tool for Platynereis dumerilii and early development of other metazoans* [Chou et al.](https://doi.org/10.1186/s12864-018-4987-0) BMC genomics 2018
  - [ ] *A transcriptional blueprint for a spiral-cleaving embryo* [Chou et al.](https://doi.org/10.1186/s12864-016-2860-6) BMC Genomics 2016


Tessmar-Raible's stuff **Kevin likely never looked at the more recent ones**:
- [ ] *Combined transcriptome and proteome profiling reveals specific molecular brain signatures for sex, maturation and circalunar clock phase* [Schenk et al.](https://elifesciences.org/articles/41556) Elife 2019 transcriptome can be found here: https://www.ebi.ac.uk/ena/browser/view/HAMN01000000 **See above supplementary file that lists accession**

### Data that raises questions

- [x] Kevin has an [incomplete assembly QC Github repo](https://github.com/mickey-spongebob/platynereis-sequencing). Do we have anything else for the paper?
    * I see jobs for Falcon and CANU that are extremely welcome here because they have list of Fastq files.
    * No other Github repo exist, and that work was stopped.
- [x] `/g/arendt/data/SmartSeq_sequencing` What is it? How is it? Temperature sensitivity test 0-5-15-30. For Fabi and phosphoproteome. Single nuclei technology.
- [x] `/g/arendt/data/platynereis_rna-seq-reads/pdum_dissected_parts` has FASTQ files Names seen are Simakov / Larsson / Vopalenski. Really old, unknown how it was generated.
- [x] `/g/arendt/data/GON2019_IM worms seq results` has FASTQ files. Gulf of Naples (Ischia) immature worms. Julieta's name is on it. Related spreadsheet: https://docs.google.com/spreadsheets/d/1HQu4Ta0AhaSLO6npPYydD3oF9qnVUiJZiwXhYloex_k
- [ ] `/g/arendt/data/genome_assemblies/pdum-v1.0/rna-seq/populations/chile` has FASTQ and Oleg's name on it. Need to ask Oleg
- [x] `/g/arendt/data/epigenome_sequencing` has a `pdum` folder. What is it? Single cell ATACseq, not looking so good. Processed by Nico, ask him. On 10x spreadsheet
- [ ] `/g/arendt/incoming`  weird data transfer folder
- [x] `/data/DNA/platynereis-dumerilii/whole-genome/pacbio/pdum_v1_EMBL_pacbio_CLR.tar.gz` has no info, from 2023.
- [x] `/data/DNA/platynereis-massiliensis/roscoff` mixed contents. Illumina Fastq, noted as collected by Conrad Helm. Shallow sequencing, Pacbio CLR2, FIRST and SECOND compressed files to look into. Multi Pacbio and Illumina runs.
- [x] `/data/RNA` folder is mostly empty. Check Kevin's Github for a list of what he used.
- [ ] Need to get some data produced by Eve Gazave

### Data to deal with

- `/g/arendt/data/transcriptome_sequencing/pdum/10x` has notably many BAM files
- Jekely 1M transcripts from [their website](https://jekelylab.ex.ac.uk/sequences/bmc_genomics_2013.html). It is stage specific which is cool here!
We have it in the lab already decompressed at `/g/arendt/data/platynereis_rna-seq-reads/pdum_life-cycle_data/file2/*` (WTF with hyphens in file paths in this lab...)
- `/g/arendt/10x_sequencing` has many libraries with fastq files


### Data folder in taped storage

There is a note about an omics collection being started in 2021. Not all of it is Platy.
Only the DNA and RNA folder are full of platy stuff.

#### DNA folder

- `/data/DNA/platynereis-dumerilii`
  - `hi-c` and `chipseq` folder are empty
  - `whole-genome` has WT lab only despite inbred folders:
     - illumina for the WT not inbred (`/data/DNA/platynereis-dumerilii/whole-genome/illumina/heidelberg/wildtype`). 150 paired end on a male whole body sample according to README. Starts in `HKNVNBBXY...`. From 2021
     - nanopore with no description (`gridion`) from 2018.
     - Pacbio HiFi from 2021 MPI Dresden collab, male sperm DNA on PacBio Sequel II. Has `(ok)ccs.fa.gz` files in `/data/DNA/platynereis-dumerilii/whole-genome/pacbio/pacbio-HiFi`
     - older Pacbio CLR files (older Pacbio) in `/data/DNA/platynereis-dumerilii/whole-genome/pacbio/pacbio-HiFi`. 
       - The `florida` part has the raw XML files in the `entrypoint` subdirs, and `fastq` outputs. First CLR from Leonid Moroz
       - the `dovetail` part has a link to a script to download files from Dovetail genomics and some `fastq.gz` files. `/data/DNA/platynereis-dumerilii/whole-genome/pacbio/pacbio_CLR/pacbio-CLR_2_dovetail_duygu`. The script has no extra info (both `fastq` and `fastq.gz` files get downloaded.)
       - `pdum_v1_EMBL_pacbio_CLR.tar.gz` is missing information from 2023. From Leonid , Florida scientist with big sequencing budget.
- `/data/DNA/platynereis-megalops` has HiFi data in `.tar.gz` format and nothing else. From 2022
- `/data/DNA/platynereis-massiliensis/roscoff` has some `.txt.gz` fastq files from Illumina (?), some SEQUEL2 CLR Pacbio stuff. Conrad Helm mentioned in README. Need help looking into. Genomic DNA

#### RNA folder

Many empty folders!

- `/data/RNA/platynereis-massiliensis`
  - `ILLUMINA` has files `.txt.gz` files from 2021
  - `/data/RNA/platynereis-massiliensis/ISOSEQ/pmassISOSEQ.m64046_210717_105606.tar.gz` from 2021 has 557GB of raw `.bam` files according to README (that's **not** raw data, no?). From 2 females.
- `/data/RNA/platynereis-dumerilii` is empty
- `/data/RNA/platynereis-megalops` is empty

## Genome/transcriptome versions

### List of genome version

- **Transcriptome assembly v1**: Done with clc / cap3. Conzelman 2013 from Jekely lab
- **Transcriptome assembly v2**: Done with Trinity / cap3. 
- **Kevin 1M**: `/g/arendt/data/transcriptome_sequencing/pdum/kevin_1million/` from 2021. Genome indices here. Built via Trinity. GTF/Fasta files
- **pdumv0.5**: `/g/arendt/data/genome_assemblies/pdumv0.5`. Output indices. `/g/arendt/data/sequencing/plat_genome_sequencing/pdumv0.5` from 2020. Genome assembly done by Oleg with Falcon. `fasta-seq` has the sequences. Kevin did transcriptome assembly using Jekely lab data
- **pdum-v1.0**: `/g/arendt/data/transcriptome_sequencing/pdum-v1.0` from May 2021 has not much details on how it was generated, but is has a fasta and gtf file
- **pdumv2**: `/g/arendt/data/transcriptome_sequencing/pdumv2` from September 2021 has an UPDATES folder. Not much details on the construction. Lookup table between 1MtXOME and XLOCs which is nice to have
- **pdumv2.1**: `/g/arendt/data/transcriptome_sequencing/pdum-v2.1` from Jan 2022. Not much details on the construction. No raw data.

### State of various genome assembly, on taped storage especially

We have the Dropbox folders mentioned before: https://www.dropbox.com/scl/fo/fv9avislnudrjd2cvif8r/ACsGxQAOm2Wiy7mGTexWPCo?rlkey=sj2pk4n723i1e78wv4o22ez6k&st=dey014kk&dl=0

### Transcriptomes from other labs WIP

### What next after the genome paper?
Hybridization? Massiliensis vs agilis one thing, but platy people seem to have 'topped up' crashing colonies with wild isolates.
This means the genome may be a mix of weird stuff, even cryptic species.
Inbred sequencing? Raible lab


## Other annelids genomes WIP

### Papers to look at

#### Platy papers with genomic raw data

Some bibtex file would be cool.
Niko points out https://platynereis.com/literature/ which makes sense.
See also https://github.com/platynereis/platynereis.github.io/blob/main/Resources/techniques.md

- 2024:
  - [ ] *Molecular profiles, sources and lineage restrictions of stem cells in an annelid regeneration model* Nature Communications 2024 [Stockinger et al.](https://doi.org/10.1038/s41467-024-54041-3). Regeneration with bulk RNA (NCBI SRA BioProject PRJNA1060927 SAMN39250368 to SAMN39250382) and single cell (NCBI SRA BioProject PRJNA1060254 SAMN39223008 to SAMN39223016)
  - [ ] *Molecular circadian rhythms are robust in marine annelids lacking rhythmic behavior* Plos Biology 2024 [Hafker et al.](https://pubmed.ncbi.nlm.nih.gov/38603542/). Data can be found on [dryad](https://datadryad.org/stash/landing/show?id=doi%3A10.5061%2Fdryad.31zcrjdnq)
    - Samples were run on a NextSeq550 System (Illumina, USA) with the High Output-kit as 75 bp paired-end reads and resulted in a total of 445,553,522 raw reads (sample range: 9,973,579 to 15,234,688 reads). 
    - We trimmed/filtered the raw reads from NextSeq sequencing using cutadapt (—nextseq-trim = 20 -m 35—pair-filter = any) removing adapters in the process [84]. This reduced read numbers to 443,393,107 reads (0.48% loss). Read quality was checked before and after trimming using FastQC [85]. Trimmed reads were mapped against a Platynereis reference transcriptome generated from trunk and regenerated tail pieces (Fig 2B). Trimmed-read fastq-files and FastQC-reports as well as transcript sequences of the reference transcriptome were deposited at the Dryad online repository (doi:10.5061/dryad.31zcrjdnq)

- 2023:
  - [ ]  *Transcriptomic landscape of posterior regeneration in the annelid Platynereis dumerilii*  BMC genomics 2023. [Paré et al.](https://bmcgenomics.biomedcentral.com/articles/10.1186/s12864-023-09602-z) Gazave paper transcriptome can be found here: https://www.ebi.ac.uk/ena/browser/view/HBZZ01000001
- 2022:
  - [ ] *Gotta Go Slow: Two Evolutionarily Distinct Annelids Retain a Common Hedgehog Pathway Composition, Outlining Its Pan-Bilaterian Core* Int J Mol Sci. 2022 [Platova et al.](https://pubmed.ncbi.nlm.nih.gov/36430788/)  has some transcriptomic data for the errantial annelid *Platynereis dumerilii* (Nereididae) and sedentarian annelid *Pygospio elegans* (Spionidae) with bioproject [url](https://www.ncbi.nlm.nih.gov/bioproject/901144) and accession number PRJNA901144.  Illumina NovaSeq 6000 paired end. : Pooled samples contained 20-25 worms. The total RNA was isolated using Quick-RNA MiniPrep. The libraries were synthesized using NEBNext Ultra Directional RNA Library Prep Kit. Number of PCR cycles was 14
- 2021 TODO 
  - [ ] *Characterization of cephalic and non-cephalic sensory cell types provides insight into joint photo- and mechanoreceptor evolution* Elife 2021 [Revilla-I-Domingo](https://elifesciences.org/articles/66144) **has another de-novo transcriptome**
  - [ ] *DNA methylation atlas and machinery in the developing and regenerating annelid Platynereis dumerilii* BMC Biology 2021 [Planques et al.](https://bmcbiol.biomedcentral.com/articles/10.1186/s12915-021-01074-5) DNA methylation toolkit. Accession numbers for P. dumerilii DNA methylation and NuRD tookit genes are GenBank MW250929 to MW250948.
  - [ ] https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8482071/ has RTqPCR as well
- 2020: TODO
  


#### Non Platy

- [ ] *Comparative transcriptomics in Syllidae (Annelida) indicates that posterior regeneration and regular growth are comparable, while anterior regeneration is a distinct process* BMC Genomics 2019 [Passos Ribeiro et al.](https://bmcgenomics.biomedcentral.com/articles/10.1186/s12864-019-6223-y) has some nice lists and comparative transcriptomics for the Syllidae *Sphaerosyllis hystrix* and *Syllis gracilis*

### Marine annelids WIP

#### Owenia fusiformis WIP

#### Capitella telata WIP

#### Pristina leydyi

#### Enchytraeus japonensis

### Others non-marine annelids WIP

## Data obtained from collaboration that should be cited

- Bruno/Mette from Tomancak early stage transcriptome, Pacbio (old methods by 2024), low BUSCO completeness, authorship issues. Files in `/g/arendt/data/transcriptome_sequencing/pdum_isoseq`
- Kevin1Million has some data from the Jekely lab
- Sylke Winkler / Martin Pippel from MPI Dresden for some 2021 Pacbio Sequel II HiFi data. DNA and RNA
- Leonid Moroz, Whitney Florida
- Conrad Helm helped gather worms
- Eve Gazave has some now published data?
- Schneider lab

